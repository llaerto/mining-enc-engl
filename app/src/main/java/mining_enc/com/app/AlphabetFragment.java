package mining_enc.com.app;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

public class AlphabetFragment extends Fragment {
    private GridView gridView;
    private ArrayList<String> myList;
    private static final String urlBegin = "http://mining-enc.com/api.php?key=app_android_1&mode=letter&name=";
    private static final String format = "&format=json";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myList = new ArrayList<>(Arrays.asList(getActivity().getResources().getStringArray(R.array.Alphabet)));
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_alphabet, container, false);
        gridView = (GridView) view.findViewById(R.id.gridView);
        gridView.setAdapter(new MyGridAdapter(myList));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int number = position + 1;
                if (number > 2)
                    number += 2;
                Intent intent = new Intent(getActivity(), AlphabetRubricsActivity.class);
                intent.putExtra(AlphabetRubricFragment.LITERAL, myList.get(position));
                intent.putExtra(AlphabetRubricFragment.URL, urlBegin + number + format);
                startActivity(intent);

            }
        });

        return view;
    }

    public class MyGridAdapter extends ArrayAdapter<String> {
        public MyGridAdapter(ArrayList<String> list) {
            super(getActivity(), 0, list);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null)
                convertView = getActivity().getLayoutInflater().inflate(R.layout.list_az_item, null);

            String AZ = myList.get(position);
            TextView textView = (TextView) convertView.findViewById(R.id.list_az_title);
            textView.setText(AZ);

            return convertView;
        }
    }
}
