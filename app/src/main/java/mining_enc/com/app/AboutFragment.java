package mining_enc.com.app;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class AboutFragment extends Fragment {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);
//        ImageView imageView = (ImageView) view.findViewById(R.id.image_about);
//        imageView.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.logo_com));
        TextView titleTextView = (TextView) view.findViewById(R.id.about_title_text);
        TextView secondTextView = (TextView) view.findViewById(R.id.about_second_text);
        TextView thirdTextView = (TextView) view.findViewById(R.id.about_third_text);
        TextView endTextView = (TextView) view.findViewById(R.id.about_end_text);
        Button siteButton = (Button) view.findViewById(R.id.button_site);
        Button voteButton = (Button) view.findViewById(R.id.button_vote_for_us);
        titleTextView.setText(R.string.about_title_textview);
        secondTextView.setText(R.string.about_second_textview);
        thirdTextView.setText(R.string.about_third_textview);
        endTextView.setText(R.string.about_end_textview);
        siteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentSite = new Intent(Intent.ACTION_VIEW);
                intentSite.setData(Uri.parse(getString(R.string.site_url)));
                startActivity(intentSite);
            }
        });
        voteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(goToMarket);
            }
        });
        return view;
    }


}
