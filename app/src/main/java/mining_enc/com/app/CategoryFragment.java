package mining_enc.com.app;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class CategoryFragment extends ListFragment {
    private ArrayList<String> listCats;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listCats = new ArrayList<>(Arrays.asList(getActivity().getResources().getStringArray(R.array.Category)));
        setListAdapter(new MyCategoriesAdapter(listCats));
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Toast.makeText(getActivity(), R.string.in_maintenance, Toast.LENGTH_SHORT).show();
    }

    public class MyCategoriesAdapter extends ArrayAdapter<String> {
        public MyCategoriesAdapter(ArrayList<String> listCats) {
            super(getActivity(), 0, listCats);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(R.layout.list_cats_item, null);
            }

            String category = getItem(position);
            TextView separatorTextView = (TextView) convertView.findViewById(R.id.list_cats_item_separator);
            separatorTextView.setText("⚫");
            TextView titleTextView = (TextView) convertView.findViewById(R.id.list_cats_item_title);
            titleTextView.setText(category);
            return convertView;
        }
    }
}
