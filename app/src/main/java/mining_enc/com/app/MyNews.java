package mining_enc.com.app;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class MyNews {
    private String name;
    private String date;
    private String url;

    public MyNews() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        SimpleDateFormat simpleDateFormatOld = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss", Locale.ENGLISH);
        SimpleDateFormat simpleDateFormatNew = new SimpleDateFormat("dd MMM");
        try {
            Date data = simpleDateFormatOld.parse(date.substring(0, date.length() - 3));
            date = simpleDateFormatNew.format(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}