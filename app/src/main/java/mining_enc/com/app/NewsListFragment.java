package mining_enc.com.app;


import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class NewsListFragment extends ListFragment {
    private MyNewsAdapter myNewsAdapter;
    private ArrayList<MyNews> list;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        list = new ArrayList<>();
        myNewsAdapter = new MyNewsAdapter(list);
        setListAdapter(myNewsAdapter);
        MyNewsAsync myNewsAsync = new MyNewsAsync();
        myNewsAsync.execute(getString(R.string.site_rss));
    }



    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Intent intentSite = new Intent(Intent.ACTION_VIEW);
        intentSite.setData(Uri.parse(list.get(position).getUrl()));
        startActivity(intentSite);
    }

    private String downloadUrl(String strUrl) throws IOException {
        StringBuilder builder = new StringBuilder("");
        java.net.URL url = new URL(strUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line = "";
        while ((line = bufferedReader.readLine()) != null) {
            builder.append(line);
        }
        connection.disconnect();
        return builder.toString();
    }

    private void createList(String xmlString, ArrayList<MyNews> myNewses) {
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser pullParser = factory.newPullParser();
            pullParser.setInput(new StringReader(xmlString));
            int eventType = pullParser.next();
            String text = "";
            MyNews myNews = new MyNews();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tag = pullParser.getName();
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tag.equals("item"))
                            myNews = new MyNews();
                        break;
                    case XmlPullParser.TEXT:
                        text = pullParser.getText();
                        text = text.replaceAll("&quot;", "");
                        break;
                    case XmlPullParser.END_TAG:
                        if (tag.equals("item")) {
                            myNewses.add(myNews);
                        } else if (tag.equals("title")) {
                            myNews.setName(text);
                        } else if (tag.equals("link")) {
                            myNews.setUrl(text);
                        } else if (tag.equals("pubDate")) {
                            myNews.setDate(text);
                        }
                        break;
                    default:
                        break;
                }
                eventType = pullParser.next();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class MyNewsAsync extends AsyncTask<String, Integer, Void> {
        String data = null;

        @Override
        protected Void doInBackground(String... params) {
            try {
                data = downloadUrl(params[0]);
                createList(data, list);
            } catch (UnknownHostException e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //TODO progressDIalog in NewsListFragment
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            myNewsAdapter.notifyDataSetChanged();
        }
    }


    private class MyNewsAdapter extends ArrayAdapter<MyNews> {
        public MyNewsAdapter(ArrayList<MyNews> list) {
            super(getActivity(), 0, list);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null)
                convertView = getActivity().getLayoutInflater().inflate(R.layout.list_news_item, null);

            MyNews myNews = getItem(position);

            TextView titleTextView = (TextView) convertView.findViewById(R.id.mynews_item_title);
            titleTextView.setText(myNews.getName());

            TextView dateTextView = (TextView) convertView.findViewById(R.id.mynews_item_date);
            dateTextView.setText(myNews.getDate());

            return convertView;
        }
    }


}
