package mining_enc.com.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

public class ArticleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.article_fragment_container);

        if (fragment == null) {
            fragment = new ArticleFragment();
            fragmentManager.beginTransaction().add(R.id.article_fragment_container, fragment).commit();
        }
    }
}
