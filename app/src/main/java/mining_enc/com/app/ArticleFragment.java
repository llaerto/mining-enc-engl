package mining_enc.com.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;


public class ArticleFragment extends Fragment {
    public static final String ARTICLE_NAME = "com.miningenc.articlefragment.ARTICLE_NAME";
    private static final String URL_NAME = "com.miningenc.articlefragment.URL_NAME";
    private static final String URL_BEGIN = "http://mining-enc.com/api.php?key=app_android_1&mode=term&name=";
    private static final String FORMAT = "&format=json";
    private String nameReal;
    private String name64;
    private MyArticle article;
    private ActionBar actionBar;
    private WebView webView;
    private boolean isInner = false;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        MyArticleAsync myArticleAsync = new MyArticleAsync();
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(R.string.progress_dialog_title);
        progressDialog.setMessage(getString(R.string.progress_dialog_message));
        progressDialog.show();
        nameReal = getActivity().getIntent().getStringExtra(ARTICLE_NAME);
        if (nameReal == null) {
            isInner = true;
            myArticleAsync.execute(getActivity().getIntent().getStringExtra(URL_NAME));
        } else {
            try {
                name64 = tobase64(nameReal);
                name64 = name64.replace("\n", "");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            myArticleAsync.execute(URL_BEGIN + name64 + FORMAT);
        }


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article, container, false);
        actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(nameReal);
        webView = (WebView) view.findViewById(R.id.webview);
        webView.setWebViewClient(new MyWebView());
        WebSettings settings = webView.getSettings();
        settings.setDefaultFontSize(16);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_without_search, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().finish();
                return true;
            case R.id.menu_item_about:
                Intent intent = new Intent(getActivity(), AboutActivity.class);
                startActivity(intent);
                return true;
            case R.id.menu_item_site:
                Intent intentSite = new Intent(Intent.ACTION_VIEW);
                intentSite.setData(Uri.parse(getString(R.string.site_url)));
                startActivity(intentSite);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private String tobase64(String basic) throws UnsupportedEncodingException {
        byte[] data = basic.getBytes("UTF-8");
        String base64 = Base64.encodeToString(data, Base64.DEFAULT);
        return base64;
    }

    private String downloadUrl(String strUrl) throws IOException {
        StringBuilder builder = new StringBuilder("");
        URL url = new URL(strUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        InputStream inputStream = connection.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        while ((line = bufferedReader.readLine()) != null) {
            builder.append(line);
        }
        connection.disconnect();
        return builder.toString();
    }

    private MyArticle getArticle(String jsonText) {
        MyArticle myArticle = new MyArticle();
        if (jsonText.equalsIgnoreCase("false")) {
            return null;
        } else if (jsonText.equals("{\"name\":\"\",\"text\":\"\"}")) {
            return null;
        }
        try {
            JSONObject jsonObject = new JSONObject(jsonText);
            String name = jsonObject.getString("name");
            String description = jsonObject.getString("text");
            description = description.replaceAll("src=\"BASE_PATH", "src=\"http://mining-enc.com/");
            description = description.replaceAll("\" />", "\" /> <br />");
            myArticle.setName(name);
            myArticle.setDescription(description);
        } catch (Exception e) {
            return null;
        }
        return myArticle;
    }

    private class MyArticleAsync extends AsyncTask<String, String, MyArticle> {
        String html = null;
        String htmlTitle = null;

        @Override
        protected MyArticle doInBackground(String... params) {
            try {
                if (!isInner) {
                    html = downloadUrl(params[0]);
                    article = getArticle(html);
                } else {
                    htmlTitle = downloadUrl(params[0]);
                    int indexOfStart = htmlTitle.indexOf("<title>");
                    int indexOfFinish = htmlTitle.indexOf("</title>");
                    String title = htmlTitle.substring(indexOfStart + 7, indexOfFinish - 22);
                    String urlInner = "http://mining-enc.com/api.php?key=app_android_1&mode=term&name=" + tobase64(title) + "&format=json";
                    urlInner = urlInner.replace("\n", "");
                    html = downloadUrl(urlInner);
                    article = getArticle(html);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return article;
        }

        @Override
        protected void onPostExecute(MyArticle myArticle) {
            if (myArticle != null) {
                webView.loadDataWithBaseURL("http://mining-enc.com", myArticle.getDescription(), "text/html", "UTF-8", null);
                actionBar.setTitle(myArticle.getName());
            } else {
                webView.loadDataWithBaseURL("http://mining-enc.com", "<center>" + getString(R.string.no_article_found) + "</center>", "text/html", "UTF-8", null);
                actionBar.setTitle(nameReal);
            }
        }
    }

    private class MyArticle {
        private String name;
        private String description;

        public MyArticle() {
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    private class MyWebView extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, final String url) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Intent intent = new Intent(getActivity(), ArticleActivity.class);
                        intent.putExtra(URL_NAME, url);
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressDialog.dismiss();
        }
    }


}
