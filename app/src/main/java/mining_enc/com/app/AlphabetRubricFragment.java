package mining_enc.com.app;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class AlphabetRubricFragment extends ListFragment {
    public static final String URL = "com.miningenc.alphabetrubricfragment.url";
    public static final String LITERAL = "com.miningenc.alphabetrubricfragment.lit";
    private ArrayList<String> list;
    private MyCategoriesAdapter adapter;
    private String lit;
    private SearchView searchView;
    private ProgressDialog progressDialog;
    private TextView textView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(R.string.progress_dialog_title);
        progressDialog.setMessage(getString(R.string.progress_dialog_message));
        progressDialog.show();
        lit = getActivity().getIntent().getStringExtra(LITERAL);
        list = new ArrayList<>();
        adapter = new MyCategoriesAdapter(list);
        setListAdapter(adapter);
        MyAsyncList myAsyncList = new MyAsyncList();
        myAsyncList.execute(getActivity().getIntent().getStringExtra(URL));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(getString(R.string.alphabet_rubric_title) + " " + lit);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_alphabet_rubric, null);
        textView = (TextView) view.findViewById(R.id.alphabet_rubric_fragment_textview);
        textView.setVisibility(View.INVISIBLE);
        textView.setText(R.string.not_yet);
        ((ViewGroup) getListView().getParent()).addView(view);
        getListView().setEmptyView(view);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_alphabet, menu);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.menu_item_search_a).getActionView();
        searchView.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Intent intent = new Intent(getActivity(), ArticleActivity.class);
                intent.putExtra(ArticleFragment.ARTICLE_NAME, query);
                startActivity(intent);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().finish();
                return true;
            case R.id.menu_item_about:
                Intent intent = new Intent(getActivity(), AboutActivity.class);
                startActivity(intent);
                return true;
            case R.id.menu_item_site:
                Intent intentSite = new Intent(Intent.ACTION_VIEW);
                intentSite.setData(Uri.parse(getString(R.string.site_url)));
                startActivity(intentSite);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent(getActivity(), ArticleActivity.class);
        intent.putExtra(ArticleFragment.ARTICLE_NAME, adapter.getItem(position));
        startActivity(intent);
    }

    private String downloadUrl(String strUrl) throws IOException {
        StringBuilder builder = new StringBuilder("");
        java.net.URL url = new URL(strUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line = "";
        while ((line = bufferedReader.readLine()) != null) {
            builder.append(line);
        }
        connection.disconnect();
        return builder.toString();
    }

    private void listConvert(String data, ArrayList<String> arrayList) {
        try {
            JSONArray jsonArray = new JSONArray(data);

            for (int i = 0; i < jsonArray.length(); i++) {
                String tolist = jsonArray.getJSONObject(i).getString("name");
                tolist = tolist.replaceAll("&quot;", "");
                arrayList.add(tolist);
            }
        } catch (Exception e) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textView.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private class MyAsyncList extends AsyncTask<String, Integer, Void> {
        String data = null;

        @Override
        protected Void doInBackground(String... params) {
            try {
                data = downloadUrl(params[0]);
                listConvert(data, list);
            } catch (UnknownHostException e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textView.setVisibility(View.VISIBLE);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            adapter.notifyDataSetChanged();
            progressDialog.dismiss();
        }
    }

    public class MyCategoriesAdapter extends ArrayAdapter<String> {
        public MyCategoriesAdapter(ArrayList<String> listCats) {
            super(getActivity(), 0, listCats);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(R.layout.list_cats_item, null);
            }

            String category = getItem(position);
            TextView separatorTextView = (TextView) convertView.findViewById(R.id.list_cats_item_separator);
            separatorTextView.setText("⚫");
            TextView titleTextView = (TextView) convertView.findViewById(R.id.list_cats_item_title);
            titleTextView.setText(category);
            return convertView;
        }

    }

    public boolean isIconified() {
        if (!searchView.isIconified()) {
            searchView.onActionViewCollapsed();
            return true;
        } else
            return false;
    }

}
