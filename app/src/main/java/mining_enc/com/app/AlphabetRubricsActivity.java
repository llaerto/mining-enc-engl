package mining_enc.com.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

public class AlphabetRubricsActivity extends AppCompatActivity {
    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alphabet_rubric);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragment = fragmentManager.findFragmentById(R.id.alphabet_rubric_fragment_container);

        if (fragment == null) {
            fragment = new AlphabetRubricFragment();
            fragmentManager.beginTransaction().add(R.id.alphabet_rubric_fragment_container, fragment).commit();
        }
    }

    @Override
    public void onBackPressed() {
        boolean isIcon = ((AlphabetRubricFragment) fragment).isIconified();
        if (!isIcon)
            super.onBackPressed();
    }
}
